var public_spreadsheet_url = 'https://docs.google.com/spreadsheets/d/1VEGpJLwIMpl__7KkIDXXc54yOhs_9vL-0SB2psvfjLQ/pubhtml',
	table = $('table.responsive'),
	thead = $('thead tr'),
    tbody = $('tbody.list'),
    pageW = $('body').width();
    options = {
	  valueNames: [],
	  page: 20,
	  plugins: [
	      ListPagination({})
	  ]
	};

function makeTable(data,tabletop){
	var main = data.Sheet1.elements,
		headers = data.Sheet1.column_names,
		header;

	headers.forEach(function(h,i){
		header = '<th id="header'+i+'" class="sort" data-sort="'+h+'">'+h+'<span class="caret">▼</span></th>'
        thead.append(header);
        options.valueNames.push(h);
    });

    main.forEach(function(m,i){
    	tr = $('<tr></tr>');
    	headers.forEach(function(d,i){
    		tr.append('<td class="'+d+'">'+m[d]+'</td>');
    	});
    	tbody.append(tr);
    });


    var userList = new List('users', options);

}




$(document).ready( function() {
	Tabletop.init({
		key: public_spreadsheet_url,
		// simpleSheet: true,
	    callback: makeTable,
	    // proxy: 'https://jac-tabletop.s3.amazonaws.com',
	    debug: true
	});

});